// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const request = require('request');
const fs = require('fs');
const ejs = require("ejs");
const json2csv = require("json2csv");
const date = require("dateformat");
// const session = require("express-session");
//  add applicaiton details
const app_name = "zunroof";
const app_id = "local.5968b77d28b969.92799388";
const app_secret = "93gJpAwtHT3o6BQaVc72208qIPakZrY14NaHIw18R1ybNKhmDP";
const auth = "";
const formidable = require('formidable');
// app
const app = express();

var countX = 0;
var start = 0;
var starting=0;
var countID = 0;
var countIDmodify = 0;
var refreshtoken = "";
// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


// defining the global variable used
var auth_token = "";
var refresh_auth_token = "";
var scope = "";
var auth_expires = 0;


// session secrete

app.use(require("express-session")({
    secret : "No one should see this",
    resave : false ,
    saveUninitialized :false
}));
var ssn;

// Catch / routes

app.get('/',function(req,res){
    ssn = req.session;          // seesion created
    ssn.count = 0;
    ssn.start = 0;              // all leads
    ssn.idcount = 0;            // all leads id
    ssn.startmodify = 0;              // modify leads
    ssn.idcountmodify = 0;            // modify leads id
    var codeURL = "https://"+app_name+".bitrix24.com/oauth/authorize/?response_type=code&client_id="+app_id+"&redirect_uri=app_URL";
    res.redirect(codeURL);  // redirect to get the auth code
})


// get bitrix send data from bitrix server
app.get('/code',function(req,res){
    if(""+ssn=="undefined"){    // session is not created
        console.log(ssn);
        res.redirect('/');
    }else{  // if(session is created)
        var url = "https://"+app_name+".bitrix24.com/oauth/token/?grant_type=authorization_code&client_id="+app_id+"&client_secret="+app_secret+"&code="+req.query.code+"&scope="+req.query.scope+"&redirect_uri=https://bitrix-karan-kumar.c9users.io/add";
        request({url:url,json:true},function(error,response,body){
            ssn.auth_token = body.access_token;
            ssn.refresh_auth_token = body.refresh_token;
            ssn.scope = body.scope;
            ssn.auth_expires = body.expires_in;
            console.log(ssn.auth_token);
        });
        setTimeout(function(){res.redirect('/option')},1000);   
    }
});


// different options
app.get('/option',function(req, res) {
    if(""+ssn=="undefined"){ // redierct if seesion is not created
      console.log('directly');
      res.redirect('/')
    }else{
      console.log(ssn)
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write('<a href = "/form">GOTO Add LEADS</a></br></br>');
      res.write('<a href = "/fetch">Fetch All LEADS</a></br></br>');
      res.write('<a href = "/fetchmodify">Fetch Modify LEADS</a>');
      return res.end();
    }
})


app.get('/form',function(req, res) {
    if(""+ssn=="undefined"){
        res.redirect('/')
    }else{
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write('<form action="fileupload" method="post" enctype="multipart/form-data">');
      res.write('<input type="file" name="filetoupload"><br>');
      res.write('<input type="submit">');
      res.write('</form>');
      return res.end();
    }
})


//upload file

app.post('/fileupload',function(req, res) {
    if(""+ssn=="undefined"){
        conosle.log('post')
        res.redirect('/');
    }else{
        // empty the file
        fs.writeFileSync('./duplicate.csv'," ",'utf8')
        fs.writeFileSync('./error.csv'," ",'utf8')
        //get uploaded data
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
          var oldpath = files.filetoupload.path;
          var newpath = './file.csv';
          fs.rename(oldpath, newpath, function (err) {
            if (err) throw err;
            else res.redirect('/add');
          });
        })
    }
    
})


// to add leads
app.get('/add', (req, res) => {
	if(""+ssn=='undefined'){
		console.log('add');
		res.redirect('/');
	}else{
		   var data = fs.readFileSync('./file.csv','utf8');
     	   var array = data.split('\n')
		   var head = array[0].split("\t");
		   var called = false;
		   var indexVal = head.indexOf("PHONE");
		   var countX = ssn.count;
		   if(countX == 0)            //if user in the midway sends the data
		   add(1);  
		   else
		   add(countX);               // start from the last added data
		   
		   //function to add multiple leads
		   function add(i){
	       if(i==array.length){
            setTimeout(function(){ res.redirect('/download')},2000);
	       }else if(i%50==0&&called==false){
		         waitFn(i);
		     }else{
                var arr = array[i].split("\t");
                responseSend(res,ssn.auth_token,arr[indexVal],arr,head,array.length-1,i);
		          setTimeout(function(){
		             console.log(i);
		              called = false;
		              i++;
		              ssn.count = i;
		              add(i);
		         },600)     
		     }
		   }

           // to regenerate the auth key
		  function waitFn(i){
		      called = true
		      countX =i+1;
		      var refreshURL = "https://"+app_name+".bitrix24.com/oauth/token/?grant_type=refresh_token&client_id="+app_id+"&client_secret="+app_secret+"&refresh_token="+ssn.refresh_auth_token+"&scope="+ssn.scope+"&redirect_uri=app_URL"
		      request({url:refreshURL,json:true},function(error,response,body){
		        ssn.refresh_auth_token = body.refresh_token;
		        ssn.auth_token = body.access_token;
		      });
		      setTimeout(function(){
		          console.log('called'+i);
		          add(i);
		      },2000);
		  }
	}
});


// to send data
function responseSend(res,token,phone,array,head,total,current){
    console.log('after')
    var auth = token;
    console.log(ssn.auth_token);
    console.log(phone)
    var urllist = "https://"+app_name+".bitrix24.com/rest/crm.lead.list?auth="+ssn.auth_token+"&filter[PHONE]="+phone;
    var str = "";
    var i =0;
    if(""+ssn.auth_token!="undefined"){
        for(i=0;i<head.length;i++){
        if(""+array[i]!='undefined'){
          if(head[i].trim()=="PHONE"||head[i].trim()=="EMAIL"){
              str += "&fields["+head[i].trim()+"][0][VALUE]="+array[i]+"&fields["+head[i].trim()+"][0][VALUE_TYPE]=HOME";
          }else{
              array[i]=array[i].replace(/^"(.+(?="$))"$/, '$1'); 
              str += "&fields["+head[i]+"]="+array[i];
          }
        }
      }
      if(i==array.length){
          i=0;
          var dataSring = JSON.stringify(array);
          trimDataString = dataSring.substring(1,dataSring.length-1);
        request({url: urllist,json: true}, function (error, response, body) {
          if(error){
            fs.appendFileSync('./error.csv',trimDataString+"\n",'utf8')
          }else{
              console.log("here");
            if(body.total == 0){
                var urlget = "https://"+app_name+".bitrix24.com/rest/crm.lead.add.json?auth="+auth+""+str;
                request({url: urlget,json: true}, function (error, response, body) {
                if(error){      // error is sending the data
                  fs.appendFileSync('./error.csv',trimDataString+"\n",'utf8')
                }else if(""+body.error!="undefined"){                                   // if there is a error from bitrix
                    fs.appendFileSync('./error.csv',trimDataString+"\n",'utf8') 
                }
                    str = "";        
                    if(current==total){
                        countX = 0;
                    }
                });
            }else if(''+body.total == 'undefined'){   // if data is not parse and body is not present
              fs.appendFileSync('./error.csv',trimDataString+"\n",'utf8')
            }else{  
                console.log(trimDataString);
                fs.appendFileSync('./duplicate.csv',trimDataString+"\n",'utf8')
                if(current==total){
                  countX=0;
                }
                console.log('duplicate');
                str = "";
            }
          }
        });
      }  
  }else{
    try{
        res.redirect('/');
    }catch(e){
        console.log(e);
    }
  }
  
}


// download duplicate leads
app.get('/download',function(req, res) {
    fs.appendFileSync('./duplicate.csv','\n\n\nerrors : \n','utf8')
    var data = fs.readFileSync('./error.csv','utf8');
    fs.appendFileSync('./duplicate.csv',data,'utf8');    //merging duplicates and errors file
    setTimeout(function(){
        res.download('./duplicate.csv');
    },5000);
});






//=================//
//      FETCH     //
//================//

app.get('/fetch',function(req, res) {
    if(""+ssn=="undefined"){
        console.log('fetchssn')
        res.redirect('/')
    }else{
        var url = 'https://'+app_name+'.bitrix24.com/rest/crm.lead.list';
        if(ssn.start==0){
            fetch();    
        }
        console.log(ssn.auth_token);
        function fetch(){
            var start = ssn.start;
            var urlValue = url+'?auth='+ssn.auth_token+"&start="+ssn.start;
            console.log(urlValue);
            var end = false;
            if(start==0)
            fs.writeFile('./id.csv',"");
            
            request({url:urlValue,json:true},function(error,response,body){
                var initial = body.result.length;
                var last = body.next;
                ssn.start = last;
                console.log(initial);
                if(!end){
                    for(var i=0;i<initial;i++){
                        if(i==initial-1&&((body.total-initial)%50==0)){
                            fs.appendFile('./id.csv',body.result[i].ID+"\t");
                            res.redirect('/fetchid');
                            countID++;    
                        }else if(i==initial-1){
                            fs.appendFileSync('./id.csv',body.result[i].ID+"\t");
                            setTimeout(function(){fetch()},200) 
                            countID++;
                        }else{
                            console.log(body.result[i].ID);
                            fs.appendFileSync('./id.csv',body.result[i].ID+"\t");   
                            countID++;
                        }
                    }   
                }
            });
        }
    }    
})



//fetch individual data

app.get('/fetchid',function(req, res) {
    if(""+ssn=="undefined"){
        console.log('fetchidssn');
        res.redirect('/');
    }else{
        var data = fs.readFileSync('./id.csv','utf8');
        var info = data.trim();
        info = info.split('\t');
        console.log(countID);
        // info.splice(-1,1);
        var url = 'https://'+app_name+'.bitrix24.com/rest/crm.lead.get';
        if(ssn.idcount==0)
        {
            console.log(info.length)
            fs.writeFile('./userDetails.csv',"",'utf8')
            idget(ssn.idcount);
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write('<a href = "/download/fetch">Click after 2hrs</a></br></br>');
            return res.end();  
        }
    
    
        // for recurrsive calling
        function idget(i){
            var urlValue = url+'?auth='+ssn.auth_token+'&id='+info[i];
            request({url:urlValue,json:true},function(error,response,body){
                console.log(JSON.stringify(body));
                fs.appendFileSync('./userDetails.csv',JSON.stringify(body.result)+",\n");   
            })
            console.log(i);
            if(i<info.length-1){
                if(i%500==0){
                    console.log(i);
                    console.log(ssn.auth_token)
                    var refreshURL = "https://"+app_name+".bitrix24.com/oauth/token/?grant_type=refresh_token&client_id="+app_id+"&client_secret="+app_secret+"&refresh_token="+ssn.refresh_auth_token+"&scope="+ssn.scope+"&redirect_uri=app_URL"
                     request({url:refreshURL,json:true},function(error,response,body){
                         ssn.refresh_auth_token = body.refresh_token;
                         ssn.auth_token = body.access_token;
                     })
                }
                //for checking only
                setTimeout(function() {
                    console.log(ssn.auth_token)
                },2000);
                setTimeout(function(){
                    i++;
                    ssn.idcount=i;
                    idget(i)
                },500)    
            }else{
                    ssn.destroy();
                setTimeout(function done(){
                    console.log('done');
                },5000);
            }
        }
    }
})


// download the fetch data
app.get('/download/fetch',function(req, res) {
        formatAllData();
        setTimeout(function(){
          res.download('./leadall.csv');
        },10000);
})


// format the json leads to csv

function formatAllData(){
    var condition = false;
    var fields = ["ID", "TITLE", "HONORIFIC", "NAME", "SECOND_NAME", "LAST_NAME", "COMPANY_TITLE", "COMPANY_ID", "CONTACT_ID", "IS_RETURN_CUSTOMER", "BIRTHDATE", "SOURCE_ID", "SOURCE_DESCRIPTION", "STATUS_ID", "STATUS_DESCRIPTION", "POST", "COMMENTS", "CURRENCY_ID", "OPPORTUNITY", "HAS_PHONE", "HAS_EMAIL", "ASSIGNED_BY_ID", "CREATED_BY_ID", "MODIFY_BY_ID", "DATE_CREATE", "DATE_MODIFY", "DATE_CLOSED", "STATUS_SEMANTIC_ID", "OPENED", "ORIGINATOR_ID", "ORIGIN_ID", "ADDRESS", "ADDRESS_2", "ADDRESS_CITY", "ADDRESS_POSTAL_CODE", "ADDRESS_REGION", "ADDRESS_PROVINCE", "ADDRESS_COUNTRY", "ADDRESS_COUNTRY_CODE", "UTM_SOURCE", "UTM_MEDIUM", "UTM_CAMPAIGN", "UTM_CONTENT", "UTM_TERM", "UF_CRM_1484372848", "UF_CRM_1484494718", "UF_CRM_1484494739", "UF_CRM_1484494781", "UF_CRM_1484495508", "UF_CRM_1484495744", "UF_CRM_1484498099", "UF_CRM_1484498113", "UF_CRM_1484661545", "UF_CRM_1484661722", "UF_CRM_1484661750", "UF_CRM_1484921990", "UF_CRM_1485014584", "UF_CRM_1485017992", "UF_CRM_1485252641", "UF_CRM_1485252656", "UF_CRM_1486105518", "UF_CRM_1488812124", "UF_CRM_1493102721", "UF_CRM_1495623199", "UF_CRM_1496231195", "UF_CRM_1496231211", "UF_CRM_1496231221", "UF_CRM_1499509452", "PHONE.ID", "PHONE.VALUE_TYPE", "PHONE.VALUE", "PHONE.TYPE_ID", "EMAIL.ID", "EMAIL.VALUE_TYPE", "EMAIL.VALUE", "EMAIL.TYPE_ID"];
    var data = fs.readFileSync('./userDetails.csv','utf8');
    var dataArray = data.split(',\n');
    for(var i=0;i<1;i++){
        console.log(i);
        console.log(""+dataArray[i]);
        var jsondata = JSON.parse(""+dataArray[i]);
        var csv = json2csv({ data: jsondata, fields: fields,unwindPath: ['PHONE', 'EMAIL']});
        var csvfile = csv.split('\n');
        fs.writeFileSync('./leadall.csv',csvfile[0]+'\n','utf8');
    }
    for(var i=0;i<dataArray.length-1;i++){
        console.log(i);
        var jsondata = JSON.parse(dataArray[i]);
        var csv = json2csv({ data: jsondata, fields: fields,unwindPath: ['PHONE', 'EMAIL']});
        var csvfile = csv.split('\n');
        fs.appendFileSync('./leadall.csv',csvfile[1]+'\n','utf8');
    }
}


//=======================//
//      FETCH MODIFY     //
//=======================//

app.get('/fetchmodify',function(req, res) {
    console.log(ssn)
     if(""+ssn=="undefined"){
         console.log('fetchmodifyssn');
        res.redirect('/')
    }else{
        var url = 'https://'+app_name+'.bitrix24.com/rest/crm.lead.list';
        if(ssn.startmodify==0){
            var now  = new Date();
            var currentDate = date(now, "isoDate");
            fetch();    
        }
        
        console.log(ssn.auth_token);
        function fetch(){
            var startmodify = ssn.startmodify;
            var urlValue = url+'?auth='+ssn.auth_token+"&filter[>DATE_MODIFY]="+currentDate+"&start="+ssn.startmodify;
            console.log(urlValue);
            var end = false;
            if(startmodify==0)
            fs.writeFile('./idmodify.csv',"");
            
            request({url:urlValue,json:true},function(error,response,body){
                var initial = body.result.length;
                var last = body.next;
                ssn.startmodify = last;
                console.log("last : "+last)
                console.log(initial);
                if(!end){
                    for(var i=0;i<initial;i++){
                        if(i==initial-1&&((body.total-initial)%50==0)){
                            fs.appendFile('./idmodify.csv',body.result[i].ID+"\t");
                            res.redirect('/fetchidmodify');
                            countIDmodify++;    
                        }else if(i==initial-1){
                            fs.appendFileSync('./idmodify.csv',body.result[i].ID+"\t");
                            setTimeout(function(){fetch()},200) 
                            countIDmodify++;
                        }else{
                            // console.log(body.result[i].ID);
                            fs.appendFileSync('./idmodify.csv',body.result[i].ID+"\t");   
                            countIDmodify++;
                        }
                    }   
                }
            });
        }
    }
})



//fetch individual data

app.get('/fetchidmodify',function(req, res) {
    if(""+ssn=="undefined"){
        console.log('fetchidmodifyssn');
        res.redirect('/');
    }else{
        var data = fs.readFileSync('./idmodify.csv','utf8');
        var info = data.trim();
        info = info.split('\t');
        console.log(countIDmodify);
        // info.splice(-1,1);
        var url = 'https://'+app_name+'.bitrix24.com/rest/crm.lead.get';
        if(ssn.idcountmodify==0)
        {
            console.log(info.length)
            fs.writeFile('./userDetailsModify.csv',"",'utf8')
            idget(ssn.idcountmodify);
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write('<a href = "/download/fetchmodify">click after 2 mins</a></br></br>');
            return res.end();  
        }
        // for recurrsive calling
        function idget(i){
            var urlValue = url+'?auth='+ssn.auth_token+'&id='+info[i];
            request({url:urlValue,json:true},function(error,response,body){
                // console.log(JSON.stringify(body));
                fs.appendFileSync('./userDetailsModify.csv',JSON.stringify(body.result)+",\n");   
            })
            console.log(i);
            if(i<info.length-1){
                if(i%500==0){
                    console.log(i);
                    console.log(ssn.auth_token)
                    var refreshURL = "https://"+app_name+".bitrix24.com/oauth/token/?grant_type=refresh_token&client_id="+app_id+"&client_secret="+app_secret+"&refresh_token="+ssn.refresh_auth_token+"&scope="+ssn.scope+"&redirect_uri=app_URL"
                     request({url:refreshURL,json:true},function(error,response,body){
                         ssn.refresh_auth_token = body.refresh_token;
                         ssn.auth_token = body.access_token;
                     })
                }
                //for checking only
                setTimeout(function() {
                    console.log(ssn.auth_token)
                },2000);
                setTimeout(function(){
                    i++;
                    ssn.idcountmodify=i;
                    idget(i)
                },500)    
            }else{
                ssn.destroy();
                setTimeout(function done(){
                    console.log('done');
                },5000);
            }
        }
    }
})


// download the fetch data
app.get('/download/fetchmodify',function(req, res) {
        formatData();
        setTimeout(function(){
          res.download('./leadmodify.csv');  
        },5000);
})


function formatData(){
    var condition = false;
    var fields = ["ID", "TITLE", "HONORIFIC", "NAME", "SECOND_NAME", "LAST_NAME", "COMPANY_TITLE", "COMPANY_ID", "CONTACT_ID", "IS_RETURN_CUSTOMER", "BIRTHDATE", "SOURCE_ID", "SOURCE_DESCRIPTION", "STATUS_ID", "STATUS_DESCRIPTION", "POST", "COMMENTS", "CURRENCY_ID", "OPPORTUNITY", "HAS_PHONE", "HAS_EMAIL", "ASSIGNED_BY_ID", "CREATED_BY_ID", "MODIFY_BY_ID", "DATE_CREATE", "DATE_MODIFY", "DATE_CLOSED", "STATUS_SEMANTIC_ID", "OPENED", "ORIGINATOR_ID", "ORIGIN_ID", "ADDRESS", "ADDRESS_2", "ADDRESS_CITY", "ADDRESS_POSTAL_CODE", "ADDRESS_REGION", "ADDRESS_PROVINCE", "ADDRESS_COUNTRY", "ADDRESS_COUNTRY_CODE", "UTM_SOURCE", "UTM_MEDIUM", "UTM_CAMPAIGN", "UTM_CONTENT", "UTM_TERM", "UF_CRM_1484372848", "UF_CRM_1484494718", "UF_CRM_1484494739", "UF_CRM_1484494781", "UF_CRM_1484495508", "UF_CRM_1484495744", "UF_CRM_1484498099", "UF_CRM_1484498113", "UF_CRM_1484661545", "UF_CRM_1484661722", "UF_CRM_1484661750", "UF_CRM_1484921990", "UF_CRM_1485014584", "UF_CRM_1485017992", "UF_CRM_1485252641", "UF_CRM_1485252656", "UF_CRM_1486105518", "UF_CRM_1488812124", "UF_CRM_1493102721", "UF_CRM_1495623199", "UF_CRM_1496231195", "UF_CRM_1496231211", "UF_CRM_1496231221", "UF_CRM_1499509452", "PHONE.ID", "PHONE.VALUE_TYPE", "PHONE.VALUE", "PHONE.TYPE_ID", "EMAIL.ID", "EMAIL.VALUE_TYPE", "EMAIL.VALUE", "EMAIL.TYPE_ID"];
    var data = fs.readFileSync('./userDetailsModify.csv','utf8');
    var dataArray = data.split(',\n');
    for(var i=0;i<1;i++){
        console.log(dataArray[i]);
        var jsondata = JSON.parse(dataArray[i]);
        var csv = json2csv({ data: jsondata, fields: fields,unwindPath: ['PHONE', 'EMAIL']});
        var csvfile = csv.split('\n');
        fs.writeFileSync('./leadmodify.csv',csvfile[0]+'\n','utf8');
    }
    for(var i=0;i<dataArray.length-1;i++){
        console.log(i);
        var jsondata = JSON.parse(dataArray[i]);
        var csv = json2csv({ data: jsondata, fields: fields,unwindPath: ['PHONE', 'EMAIL']});
        var csvfile = csv.split('\n');
        fs.appendFileSync('./leadmodify.csv',csvfile[1]+'\n','utf8');
    }
}


/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));