var status = true;
var lastValue;
var	lastClick;
var click = 0;
// to get progress value
$('#progressbar li').on('click' , function(){
	var progressBarPosition = $(this).index()+1;
	progressDisplayChange(progressBarPosition);
});

//responsible change data 
$('#responsible tr td').on('click',function(){
	console.log()
	if(click == 0){
			lastClick = $(this);
			if(status == 'true'	){
			var value = $(this)[0].innerText;
			lastValue = value; 
			$(this)[0].innerHTML = '<input id = "responsibleTemp" type="text" name="name" placeholder="'+value+'" autocomplete="off" value="'+value+'">';
			click++;
		}
	}else if(lastClick.parent('tr').index() == $(this).parent('tr').index()){
			status = false;
	}else{
			lastClick[0].innerText = lastValue
			click = 0;
			status = true;
	}
})

$("#responsible tr td").keypress(function(){
	if(event.which === 13)
	{
		$(this)[0].innerHTML = $('#responsibleTemp')[0].value;
		status = true;
		click = 0;
	}
});

// to change the progress Bar

function progressDisplayChange(n){
	var l = $('#progressbar li').length;
	for (var i = 0; i < l; i++) {
		if(i<n-1||n==l){
			$( "#progressbar li:eq("+i+")" ).removeClass('active');
			$( "#progressbar li:eq("+i+")" ).addClass('done');
		}else if(i==n-1){
			$( "#progressbar li:eq("+i+")" ).addClass('active');
			$( "#progressbar li:eq("+i+")" ).removeClass('done');
		}else{
			$( "#progressbar li:eq("+i+")" ).removeClass('done');
			$( "#progressbar li:eq("+i+")" ).removeClass('active');
		}
	}
}
