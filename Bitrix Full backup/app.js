// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const request = require('request');
const fs = require('fs');
const ejs = require("ejs");
const json2csv = require("json2csv");
const date = require("dateformat");
const app_name = "zunroof";
const app_id = "local.5979c637b659d9.89731463";
const app_secret = "Jd8g0HZMPTuS0sViRpbcoEm9Ldv36xw2t6J3jKSdRoYPcc00Nw";
const auth = "";
const formidable = require('formidable');
const schedule = require('node-schedule');
// app
const app = express();

var processRun = false;
var countX = 0;
var start = 0;
var starting=0;
var countID = 0;
var countIDmodify = 0;
var refreshtoken = "";
// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


// defining the global variable used
var auth_token = "";
var refresh_auth_token = "";
var scope = "";
var auth_expires = 0;


// bitrix custon fields
var bitrixFields = {
    ID:"BitrixID",
    TITLE:"TITLE",
    HONORIFIC:"HONORIFIC",
    NAME:"NAME",
    SECOND_NAME:"SECOND_NAME",
    LAST_NAME:"LAST_NAME",
    COMPANY_TITLE:"COMPANY_TITLE",
    COMPANY_ID:"COMPANY_ID",
    CONTACT_ID:"CONTACT_ID",
    IS_RETURN_CUSTOMER:"IS_RETURN_CUSTOMER",
    BIRTHDATE:"BIRTHDATE",
    SOURCE_ID:"SOURCE_ID",
    SOURCE_DESCRIPTION:"SOURCE_DESCRIPTION",
    STATUS_ID:"STATUS_ID",
    STATUS_DESCRIPTION:"STATUS_DESCRIPTION",
    POST:"POST",
    COMMENTS:"COMMENTS",
    CURRENCY_ID:"CURRENCY_ID",
    OPPORTUNITY:"OPPORTUNITY",
    HAS_PHONE:"HAS_PHONE",
    HAS_EMAIL:"HAS_EMAIL",
    ASSIGNED_BY_ID:"ASSIGNED_BY_ID",
    CREATED_BY_ID:"CREATED_BY_ID",
    MODIFY_BY_ID:"MODIFY_BY_ID",
    DATE_CREATE:"DATE_CREATE",
    DATE_MODIFY:"DATE_MODIFY",
    DATE_CLOSED:"DATE_CLOSED",
    STATUS_SEMANTIC_ID:"STATUS_SEMANTIC_ID",
    OPENED:"OPENED",
    ORIGINATOR_ID:"ORIGINATOR_ID",
    ORIGIN_ID:"ORIGIN_ID",
    ADDRESS:"ADDRESS",
    ADDRESS_2:"ADDRESS_2",
    ADDRESS_CITY:"ADDRESS_CITY",
    ADDRESS_POSTAL_CODE:"ADDRESS_POSTAL_CODE",
    ADDRESS_REGION:"ADDRESS_REGION",
    ADDRESS_PROVINCE:"ADDRESS_PROVINCE",
    ADDRESS_COUNTRY:"ADDRESS_COUNTRY",
    ADDRESS_COUNTRY_CODE:"ADDRESS_COUNTRY_CODE",
    UTM_SOURCE:"UTM_SOURCE",
    UTM_MEDIUM:"UTM_MEDIUM",
    UTM_CAMPAIGN:"UTM_CAMPAIGN",
    UTM_CONTENT:"UTM_CONTENT",
    UTM_TERM:"UTM_TERM",
   UF_CRM_1484372848:"Active",
    UF_CRM_1484494718:"Last_Action_Date",
    UF_CRM_1484494739:"Next_Action_Date",
    UF_CRM_1484494781:"Next_Action",
    UF_CRM_1484495508:"Product",
    UF_CRM_1484495744:"Property_Type",
    UF_CRM_1484498099:"DB_Input",
    UF_CRM_1484498113:"Last_Actions",
    UF_CRM_1484661545:"Language",
    UF_CRM_1484661722:"Added_on",
    UF_CRM_1484661750:"High_Value",
    UF_CRM_1484921990:"Site_Assessment_Paid",
    UF_CRM_1485014584:"Monthly_Bill_Form",
    UF_CRM_1485017992:"Site_Assessment_Date",
    UF_CRM_1485252641:"Address_New",
    UF_CRM_1485252656:"Zip_Code",
    UF_CRM_1486105518:"Quote_ID",
    UF_CRM_1488812124:"SA_Fee_reaction",
    UF_CRM_1493102721:"State",
    UF_CRM_1494934411:"SA_Report",
    UF_CRM_1495623199:"Campaign_Name",
    UF_CRM_1496231195:"ad_keyword",
    UF_CRM_1496231211:"ad_matchtype",
    UF_CRM_1496231221:"ad_device",
    UF_CRM_1499509452:"Region",
    PHONE : "PHONE",
    EMAIL : "EMAIL"
  }


// session secrete

app.use(require("express-session")({
    secret : "No one should see this",
    resave : false ,
    saveUninitialized :false
}));
var ssn;



// DB setup


// DB models



// Catch / routes

app.get('/',function(req,res){
    console.log('enter');
    var codeURL = "https://"+app_name+".bitrix24.com/oauth/authorize/?response_type=code&client_id="+app_id+"&redirect_uri=app_URL";
    res.redirect(codeURL);  // redirect to get the auth code
})


// get bitrix send data from bitrix server
app.get('/code',function(req,res){
    console.log('enter');
    var url = "https://"+app_name+".bitrix24.com/oauth/token/?grant_type=authorization_code&client_id="+app_id+"&client_secret="+app_secret+"&code="+req.query.code+"&scope="+req.query.scope+"&redirect_uri=https://bitrix-karan-kumar.c9users.io/add";
    request({url:url,json:true},function(error,response,body){
        auth_token=body.access_token;
        refresh_auth_token = body.refresh_token;
        scope = body.scope;
        console.log(auth_token);
        someX();
        setTimeout(function(){
            // fetchModify();
            fetch();
        },200);
    });
});


function someX(){
    console.log(new Date());
}


// to regenerate the token
var j = schedule.scheduleJob('0 42 * * * *', function(){
   
        var url = "https://"+app_name+".bitrix24.com/oauth/token/?grant_type=refresh_token&client_id="+app_id+"&client_secret="+app_secret+"&refresh_token="+refresh_auth_token+"&scope="+scope+"&redirect_uri=https://bitrix-karan-kumar.c9users.io";
        request({url:url,json:true},function(error,response,body){
            auth_token=body.access_token;
            if(!processRun){
            refresh_auth_token = body.refresh_token;
            scope = body.scope;
            console.log("42 : "+auth_token);
            }
        });
});
var j = schedule.scheduleJob('0 12 * * * *', function(){
        var url = "https://"+app_name+".bitrix24.com/oauth/token/?grant_type=refresh_token&client_id="+app_id+"&client_secret="+app_secret+"&refresh_token="+refresh_auth_token+"&scope="+scope+"&redirect_uri=https://bitrix-karan-kumar.c9users.io";
        request({url:url,json:true},function(error,response,body){ 
            auth_token=body.access_token;
        if(!processRun){ 
            refresh_auth_token = body.refresh_token;
            scope = body.scope;
            console.log("12 : "+auth_token);
             }
        });
});

//============//
//  DATABASE  //
//============//


var database1 = schedule.scheduleJob('0 0 3 * * 5', function(){
    fetchModify();
});

var database2 = schedule.scheduleJob('0 0 3 * * 2', function(){
    fetchModify();
});

var enter = false;
function fetch(){
    processRun = true;
    var url = 'https://'+app_name+'.bitrix24.com/rest/crm.lead.list';
    var urlValue = url+'?auth='+auth_token+"&start="+start;
    console.log(urlValue);
    if(start==0)
    fs.writeFile('./id.csv',"",'utf8');
    
    request({url:urlValue,json:true},function(error,response,body){
        var initial = body.result.length;
        var last = body.next;
        start = last;
        console.log(initial);
        console.log(start%1000!=0 || enter);
        if(start%1000!=0 || enter){
            enter = false;
            for(var i=0;i<initial;i++){
                if(i==initial-1&&((body.total-initial)%50==0)){
                    fs.appendFile('./id.csv',body.result[i].ID);
                    start=0;
                    countID++;
                    idToDB('id.csv');
                }else if(i==initial-1){
                    fs.appendFileSync('./id.csv',body.result[i].ID+"\n");
                    setTimeout(function(){fetch()},2000) 
                    countID++;
                }else{
                    console.log(body.result[i].ID);
                    fs.appendFileSync('./id.csv',body.result[i].ID+"\n");   
                    countID++;
                }
            }
        }else{
                enter = true;
                var url = "https://"+app_name+".bitrix24.com/oauth/token/?grant_type=refresh_token&client_id="+app_id+"&client_secret="+app_secret+"&refresh_token="+refresh_auth_token+"&scope="+scope+"&redirect_uri=https://bitrix-karan-kumar.c9users.io";
                    request({url:url,json:true},function(error,response,body){
                        auth_token=body.access_token;
                        refresh_auth_token = body.refresh_token;
                        scope = body.scope;
                    });
                   setTimeout(function(){fetch()},2000) 
            }   
    });
}



// idToDB
function idToDB(fileName){
    var d =  new Date();
    var date = d.toLocaleDateString('en-US');
    var data = fs.readFileSync('./'+fileName,'utf8');
    var dataArray = data.split('\n');
    console.log(data)
    console.log(dataArray.length);
    bitrixAllData();
}

var idcount = 0;
function bitrixAllData(){
    if(idcount==0)
        {
            idget(idcount);
        }
    }
// for recurrsive calling
function idget(i){
    var data = fs.readFileSync('./id.csv','utf8');
    var info = data.trim();
    info = info.split('\n');
    var url = 'https://'+app_name+'.bitrix24.com/rest/crm.lead.get';
    var urlValue = url+'?auth='+auth_token+'&id='+info[i];
    request({url:urlValue,json:true},function(error,response,body){
        // var sql = "INSERT INTO customers (name, address) VALUES ('Company Inc', 'Highway 37')";
        var json  = body.result;
        var dataJSON = {};
        var dataString = "{";
        for(var i in json){
            if(i=='PHONE'||i=='EMAIL'){
                var arr = [];
                for(var j=0;j<json[i].length;j++){
                    var ar1 = {VALUE:json[i][j].VALUE};
                    arr.push(ar1);
                    // console.log(bitrixFields[i]+" : "+json[i][j].VALUE);   
                }   
                dataString += "\""+bitrixFields[i]+"\":"+JSON.stringify(arr)+",";
            }else if(i=='UF_CRM_1484498099'){
                dataString += "\""+bitrixFields[i]+"\":\"\",";
            }else{
                if(""+json[i]!='undefined'&&""+json[i]!='null'){
                    var x = ""+json[i];
                    var y1 =x.replace(/\"/g, "");
                    var y =y1.replace(/\\/g, "");
                    var z =y.replace(/(?:\r\n|\r|\n)/g, '<br />');
                    dataString += "\""+bitrixFields[i]+"\":\""+z+"\",";       
                }
                
                // console.log(bitrixFields[i]+" : "+json[i]); 
            }
        }
        dataString += "\"";
        dataString = dataString.substring(0,dataString.length-2);
        dataString += "}";
        console.log(dataString);    
    })
    console.log(i);
    if(i<info.length){
        if(i%500==0){
            console.log(i);
            console.log(auth_token)
            var refreshURL = "https://"+app_name+".bitrix24.com/oauth/token/?grant_type=refresh_token&client_id="+app_id+"&client_secret="+app_secret+"&refresh_token="+refresh_auth_token+"&scope="+scope+"&redirect_uri=app_URL"
            request({url:refreshURL,json:true},function(error,response,body){
                refresh_auth_token = body.refresh_token;
                auth_token = body.access_token;
            })
        }
        //for checking only
        setTimeout(function() {
            console.log(auth_token)
        },2000);
        setTimeout(function(){
            i++;
            idget(i)
        },500)    
    }else{
        setTimeout(function done(){
            idcount=0;
            processRun = false;
            console.log('done');
        },5000);
    }
}


//================//
// MODIFY LEADS   //
//================//
var startModify = 0;
var enterModify = false;
function fetchModify(){
    processRun = true;
    var url = 'https://'+app_name+'.bitrix24.com/rest/crm.lead.list';
    var now = new Date();
    var currentDate = formatDate(now);
    console.log(currentDate);
    var urlValue = url+'?auth='+auth_token+"&filter[>DATE_MODIFY]="+currentDate+"&start="+startModify;
    console.log(urlValue);
    var end = false;
    if(startModify==0)
    fs.writeFile('./idmodify.csv',"",'utf8');
    
    request({url:urlValue,json:true},function(error,response,body){
        var initial = body.result.length;
        var last = body.next;
        startModify = last;
        console.log(initial);
        if(startModify%1000!=0||enterModify){
            enterModify = false;
            for(var i=0;i<initial;i++){
                if(i==initial-1&&((body.total-initial)%50==0)){
                    fs.appendFile('./idmodify.csv',body.result[i].ID);
                    startModify=0;
                    countID++;
                    idModifyToDB('idmodify.csv');
                }else if(i==initial-1){
                    fs.appendFileSync('./idmodify.csv',body.result[i].ID+"\n");
                    setTimeout(function(){fetchModify()},200) 
                    countID++;
                }else{
                    console.log(body.result[i].ID);
                    fs.appendFileSync('./idmodify.csv',body.result[i].ID+"\n");   
                    countID++;
                }
            }   
        }else{
            enterModify = true;
            var url = "https://"+app_name+".bitrix24.com/oauth/token/?grant_type=refresh_token&client_id="+app_id+"&client_secret="+app_secret+"&refresh_token="+refresh_auth_token+"&scope="+scope+"&redirect_uri=https://bitrix-karan-kumar.c9users.io";
                request({url:url,json:true},function(error,response,body){
                    auth_token=body.access_token;
                    refresh_auth_token = body.refresh_token;
                    scope = body.scope;
                });
            setTimeout(function(){fetchModify()},200);
        }
    });
}



// idToDB
function idModifyToDB(fileName){
    var d =  new Date();
    var date = d.toLocaleDateString('en-US');
    var data = fs.readFileSync('./'+fileName,'utf8');
    var dataArray = data.split('\n');
    console.log(data)
    console.log(dataArray.length);
    bitrixModifyData();
}

var idModifycount = 0;
function bitrixModifyData(){
    if(idModifycount==0)
        {
            idModifyGet(idcount);
        }
    }
// for recurrsive calling
function idModifyGet(i){
    var data = fs.readFileSync('./idmodify.csv','utf8');
    var info = data.trim();
    info = info.split('\n');
    var url = 'https://'+app_name+'.bitrix24.com/rest/crm.lead.get';
    var urlValue = url+'?auth='+auth_token+'&id='+info[i];
    request({url:urlValue,json:true},function(error,response,body){
        // var sql = "INSERT INTO customers (name, address) VALUES ('Company Inc', 'Highway 37')";
        var json  = body.result;
        var dataJSON = {};
        var dataString = "{";
        for(var i in json){
            if(i=='PHONE'||i=='EMAIL'){
                var arr = [];
                for(var j=0;j<json[i].length;j++){
                    var ar1 = {VALUE:json[i][j].VALUE};
                    arr.push(ar1);
                    // console.log(bitrixFields[i]+" : "+json[i][j].VALUE);   
                }   
                dataString += "\""+bitrixFields[i]+"\":"+JSON.stringify(arr)+",";
            }else if(i=='UF_CRM_1484498099'){
                dataString += "\""+bitrixFields[i]+"\":\"\",";
            }else{
                if(""+json[i]!='undefined'&&""+json[i]!='null'){
                    var x = ""+json[i];
                    var y1 =x.replace(/\"/g, "");
                    var y =y1.replace(/\\/g, "");
                    var z =y.replace(/(?:\r\n|\r|\n)/g, '<br />');
                    dataString += "\""+bitrixFields[i]+"\":\""+z+"\",";       
                }
            }
        }
        dataString += "\"";
        dataString = dataString.substring(0,dataString.length-2);
        dataString += "}";
        console.log(dataString);
        idModifycount++;
    })
    console.log(i);
    if(i<info.length){
        if(i%500==0){
            console.log(i);
            console.log(auth_token)
            var refreshURL = "https://"+app_name+".bitrix24.com/oauth/token/?grant_type=refresh_token&client_id="+app_id+"&client_secret="+app_secret+"&refresh_token="+refresh_auth_token+"&scope="+scope+"&redirect_uri=app_URL"
            request({url:refreshURL,json:true},function(error,response,body){
                refresh_auth_token = body.refresh_token;
                auth_token = body.access_token;
            })
        }
        //for checking only
        setTimeout(function() {
            console.log(auth_token)
        },2000);
        setTimeout(function(){
            i++;
            idModifyGet(i)
        },500)    
    }else{
        setTimeout(function done(){
            idModifycount = 0;
            processRun = false;
            console.log('done');
        },5000);
    }
}




function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
}


// var os = require('os');

// app.get('/',function(req,res){
//     var interfaces = os.networkInterfaces();
// var addresses = [];
// for (var k in interfaces) {
//     for (var k2 in interfaces[k]) {
//         var address = interfaces[k][k2];
//         if (address.family === 'IPv4' && !address.internal) {
//             addresses.push(address.address);
//         }
//     }
// }

// console.log(addresses);
// })





/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));