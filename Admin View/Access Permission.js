var employee = ['x','y','z'];
employeeAcess();
var table = document.getElementById('roles')
table.innerHTML = '<tr style="border-bottom: 1px solid #ddd;"><th></th><th>CRM ROLE</th><th><button id="addUser" class="glyphicon glyphicon-plus"></button></th></tr>';

function deleteRow(row){       
    var del = row.parentNode.parentNode.rowIndex;
    document.getElementById('roles').deleteRow(del);
    changeRoleDropdown();       
}

function deleteRowRole(row){       
    var del = row.parentNode.parentNode.rowIndex;
    document.getElementById('role-manage').deleteRow(del); 
    changeRoleDropdown();      
}

$('#addUser').click(function () {
    employeeAcess();              
});    



function changeRoleDropdown(){
  var l = $('#roles tr').length;
  for (var i = 0; i < l-1; i++) {
    var j = 0;
    $('#roles tr td:nth-child(2)').find('select').each(function(){})[i].innerHTML = "";
      $('#role-manage').find('tr td').each(function(){
          if(j%2==0){
            var option = document.createElement('option');
            option.innerHTML = $(this)[0].innerHTML;
            option.setAttribute('value',$(this)[0].innerHTML) 
            option.setAttribute('style','padding: 10px;');
            $('#roles tr td:nth-child(2)').find('select').each(function(){})[i].appendChild(option);
          }
          j++;
      }) 
  }  
}


function employeeAcess(){
  var table = document.getElementById('roles')
      var tr = document.createElement('tr');
      tr.setAttribute('id','info-row');
      var td1 = document.createElement('td');
      td1.className = 'roles-changeable';
      td1.setAttribute('contenteditable','true'); 
      td1.setAttribute('spellcheck','false'); 
      td1.innerHTML = 'name';
      var td2 = document.createElement('td');
      var select = document.createElement('select');
      select.className = 'form-control';
      var j = 0;
      $('#role-manage').find('tr td').each(function(){
          if(j%2==0){
            var option = document.createElement('option');
            option.innerHTML = $(this)[0].innerHTML;
            option.setAttribute('value',$(this)[0].innerHTML) 
            option.setAttribute('style','padding: 10px;');
            select.appendChild(option);
          }
          j++;
      })
      var td3 = document.createElement('td');
      var button2 = document.createElement('button');
      button2.setAttribute('id','deleteButton');
      button2.setAttribute('onclick','deleteRow(this)');
      button2.className = 'glyphicon glyphicon-remove';
      // append all newly created elements
      table.appendChild(tr);
      tr.appendChild(td1);
      tr.appendChild(td2);
      tr.appendChild(td3);
      td2.appendChild(select);
      td3.appendChild(button2);
}


$(document).ready(function(){
  $('#role-manage').on('click','tr td',function(){
    var contents = $(this).html();
    $(this).blur(function() {
        if (contents!=$(this).html()){
          changeRoleDropdown();
        }
    });
  })
})

$(document).ready(function () {
        $('#addRole').click(function () {
              var count = 1, first_row = $('#infoRowManage');
                while(count--> 0)
                	first_row.clone().appendTo('#role-manage');
          changeRoleDropdown();      
     });    
 });